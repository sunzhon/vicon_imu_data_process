# Drop Landing Dataset

This folder contains python scripts to pre-process and visualize the drop landing experiment data.

- wearable_math.py and wearable_tool.py load csv files from Xsen (IMU), Vicon, V3D, sage. These equipments provided the raw data.
- load_rawdata.py synchronize and save the IMU data, vicon data, v3D data into a hd5f file for model training of deep learning 
- process_rawdata.py visualize the synchonized dataset
- const.py defines the constant variable, paramters, data file paths, etc.
- dataset.py manage and split the data for model training.
- data folder contains the dataset and subject information

# How to use this repository

1. go to const.py to define DATA_PATH properly. DATA_PATH define the path in which the dataset (feature_lablels_rawdatasets.hdf5) was stored
2. go to process_rawdata.py to display the dataset according to your demands.
    - the dataset was stored with this dictory structure: {subject_name: [trial_1, trial_2, ...], subject_name2: [trial_1, trial_2,...], ...}
    - 'subject_name_01' is a subject's name
    - 'trial_01' represents a trial which is a two dimensional numpy array, the row is the frame, the columns represent the data fields
    - the columns can be seen in the attribution of the hdf5 data.
    - read_rawdata() in process_rawdata.py is an example python script to read the hdf5 file
 
*NOTE: A minimal version of the dataset was stored in "./data/" for public use. To use the dataset in "./data/", please specify the DATA_PATH in const.py to: DATA_PATH = os.path.join(os.path.dirname(os.path.abspath(__file__)),'data/drop_landing')*
