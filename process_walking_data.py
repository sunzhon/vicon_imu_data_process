import re
import copy
import sys
import json
import pandas as pd
import numpy as np
import h5py
import os
import pdb
current_dir = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.join(current_dir, ".."))


#if __name__ == "__main__":
#    from const import DATA_PATH, SAMPLE_FREQUENCY
#    from process_landing_data import *
#    from const import FEATURES_FIELDS, LABELS_FIELDS
#
#else:
from vicon_imu_data_process import const
from vicon_imu_data_process.const import DATA_PATH, SAMPLE_FREQUENCY
from vicon_imu_data_process.process_landing_data import *
from vicon_imu_data_process.const import FEATURES_FIELDS, LABELS_FIELDS

MAX_VALUE_LIMIT=2000
'''
Translate data fields

'''

def interpret_data_fields(file_name):
    # getting data fields
    with h5py.File(os.path.join(DATA_PATH,file_name), 'r') as hf:
        data_fields = json.loads(hf.attrs['columns'])
    
    interpreted_data_fields = copy.deepcopy(data_fields)
    for idx, field in enumerate(data_fields):
        # translate imu data fields
        if(re.search(r'X_|Y_|Z_|Quat', field)!=None):
            y = field.split('_')
            temp = y[1:];temp.append(y[0])
            w = '_'.join(temp)
            l = re.search(r'X|Y|Z|[1-4]$',w).group()
            result = w[:-1]+'_'+l
            interpreted_data_fields[idx] = result
        # translate knee moment
        if(re.search(r'EXT_KM', field)!=None):
            l = re.search(r'_X|_Y|_Z$',field).group()
            result = 'R_KNEE_MOMENT'+l
            interpreted_data_fields[idx] = result
        # translate groudn reaction force data field
        if(re.search(r'_force_', field)!=None):
            direction = re.search(r'x|y|z$',field).group()
            left_right_feet = re.search(r'[1|2]',field).group()
            result = ('L_GRF_' if left_right_feet == '1' else 'R_GRF_') + direction.upper()
            interpreted_data_fields[idx] = result
        
    return interpreted_data_fields


# find the strange values in the walking data: such as nan, null, etc
def checking_data(subjects_trials_data):
    print('START TO CHECKING DATA')
    print("---->")
    #1) get the name of subjects and trials 
    subjects_trials = {subject_id_name: subjects_trials_data[subject_id_name].keys() for subject_id_name in subjects_trials_data.keys()} # get subjects_tials
    subject_ids_names = subjects_trials.keys() # get subjects
    # 2) 遍历数据
    find_error=False
    for subject_id_name in subject_ids_names:
        for trial in subjects_trials[subject_id_name]:
            x = subjects_trials_data[subject_id_name][trial].values
            if(np.isnan(x).any()): # has nan or not
                print("NAN in these subjects and their trials:",subject_id_name, trial)
                print("NAN position:", np.argwhere(np.isnan(x)))
                find_error=True
            max_x = np.max(x)
            if(max_x>MAX_VALUE_LIMIT):
                print(subject_id_name,trial, 'strange max value:', max_x)
                find_error=True
            strange = subjects_trials_data[subject_id_name][trial].values[:,1]

    if(find_error==True):
        print('FIND ERRROR IN DATA!')
    else:
        print('NOT FOUND ERRROR IN DATA!')
    print('CHECKING DONE!')


'''
Translate the walking data format into drop landing data format

'''

def interpret_data_format(file_name, selected_data_fields,frame_number=152):
    interpreted_subjects_trials_data = {}
    
    with h5py.File(os.path.join(DATA_PATH,file_name), 'r') as hf:
        data_all_sub = {subject: subject_data[:,:frame_number,:] for subject, subject_data in hf.items()}
        data_fields = json.loads(hf.attrs['columns'])
    
    interpreted_data_fields = interpret_data_fields(file_name)
    selected_data_fields_index = []
    for field in selected_data_fields:
        selected_data_fields_index.append(interpreted_data_fields.index(field))
    
    for subject_id_name, subject_data in data_all_sub.items():
        interpreted_subjects_trials_data[subject_id_name] = {}
        for trial_idx in range(subject_data.shape[0]):
            a_trial_data = subject_data[trial_idx, :, selected_data_fields_index].T
            if(np.isnan(a_trial_data).any()):# has nan in data
                print("Tanslate NaN to zero of subject {} in trila {}".format(subject_id_name,trial_idx))
                a_trial_data = np.nan_to_num(a_trial_data,nan=0.0,posinf=0.0,neginf=0.0)
            max_value = np.max(np.abs(a_trial_data))
            if(max_value > MAX_VALUE_LIMIT): #strange value with two max value
                print("Stanrage value with large value of subject {} in trial {} in position {}. It was set to zero".format(subject_id_name, trial_idx, np.argwhere(np.abs(a_trial_data)==max_value)))
                print("strange value is {}".format(max_value))
                a_trial_data = np.where(np.abs(a_trial_data)> MAX_VALUE_LIMIT, 0.0, a_trial_data)

            pd_a_trial_data = pd.DataFrame(data=a_trial_data,columns=selected_data_fields)
            time = np.linspace(0,a_trial_data.shape[0]/SAMPLE_FREQUENCY,a_trial_data.shape[0])
            pd_a_trial_data['TIME']  = time
            pd_a_trial_data['R_GRF_Z'] = -1.0 * pd_a_trial_data['R_GRF_Z']
            interpreted_subjects_trials_data[subject_id_name][str(trial_idx)] = pd_a_trial_data
            
    return interpreted_subjects_trials_data

'''

'''

if __name__=='__main__':
    if True: # generate walking data with grf and kam
        # transfer walking experiment data into drop landing experiment data format
        file_name = 'all_17subjects_walking_data.hdf5'
        labels_names = ['R_GRF_Z','R_KNEE_MOMENT_X']
        selected_data_fields = const.extract_imu_fields(const.IMU_SENSOR_LIST,const.ACC_GYRO_FIELDS) + labels_names
        reformat_subjects_trials_data = interpret_data_format(file_name=file_name, selected_data_fields=selected_data_fields, frame_number=80)
        # checking the dataset having wrong data, such as nan, too large value
        checking_data(reformat_subjects_trials_data)
        # saving data as a hdf5 file
        save_subjects_dataset_to_a_h5(reformat_subjects_trials_data,"walking_data.hdf5")     


    if False: #noramlize data of grf
        selected_data_fields = ['TIME'] + const.extract_imu_fields(const.IMU_SENSOR_LIST,const.ACC_GYRO_FIELDS) + ['R_GRF_Z']
        subjects_trials_data = load_subjects_dataset(h5_file_name = os.path.join(DATA_PATH,"walking_data.hdf5"),selected_data_fields=selected_data_fields)
        scaled_subjects_trials_data, scaler = normalize_dataset(subjects_trials_data)
        save_subjects_dataset_to_a_h5(scaled_subjects_trials_data,'grf_norm_walking_data.hdf5')
        save_scaler(scaler,'grf_walking_scaler_file')


    if True: #noramlize data of kam
        selected_data_fields = ['TIME'] + const.extract_imu_fields(const.IMU_SENSOR_LIST,const.ACC_GYRO_FIELDS) + ['R_KNEE_MOMENT_X']
        subjects_trials_data = load_subjects_dataset(h5_file_name = os.path.join(DATA_PATH,"walking_data.hdf5"), selected_data_fields=selected_data_fields)
        scaled_subjects_trials_data, scaler = normalize_dataset(subjects_trials_data)
        save_subjects_dataset_to_a_h5(scaled_subjects_trials_data,'kam_norm_walking_data.hdf5')
        save_scaler(scaler,'kam_walking_scaler_file')
