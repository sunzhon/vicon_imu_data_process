#Python

"""
Description:
    This module transfer normalized raw data into dataset for training and testing
        
Author: Sun Tao
Email: suntao.hn@gmail.com
Date: 2022-04-09

"""
import pandas as pd
import os
import h5py
import re

import numpy as np

import tensorflow as tf

from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler
from sklearn.metrics import r2_score

from matplotlib import gridspec

import yaml
import pdb
import re
import warnings
import termcolor
import matplotlib._color_data as mcd

import datetime


if __name__ == "__main__":
    from const import SEGMENT_DEFINITIONS, SUBJECTS, STATIC_TRIALS, DYNAMIC_TRIALS,TRIALS, SESSIONS, DATA_PATH, \
            SUBJECT_HEIGHT, SUBJECT_WEIGHT, SUBJECT_ID, TRIAL_ID, XSEN_IMU_ID, IMU_DATA_FIELDS, FORCE_DATA_FIELDS,\
            KNEE_DATA_FIELDS, WRONG_TRIALS
else:
    from vicon_imu_data_process.const import SEGMENT_DEFINITIONS, SUBJECTS, STATIC_TRIALS, DYNAMIC_TRIALS,TRIALS, SESSIONS, DATA_PATH, \
            SUBJECT_HEIGHT, SUBJECT_WEIGHT, SUBJECT_ID, TRIAL_ID, XSEN_IMU_ID, IMU_DATA_FIELDS, FORCE_DATA_FIELDS,\
            KNEE_DATA_FIELDS, WRONG_TRIALS, DROPLANDING_PERIOD



'''
Packing data into windows 


'''

def windowed_dataset(series, hyperparams,shuffle_buffer=1000):
    window_size = hyperparams['window_size']
    batch_size = hyperparams['batch_size']
    shift_step = hyperparams['shift_step']
    labels_num = hyperparams['labels_num']


    #if window_size = a_trial_data.shape[0], shift_step = window_size # this is to avoid the data from different trials in a window.
    # series = tf.expand_dims(series, axis=-1)
    ds = tf.data.Dataset.from_tensor_slices(series)
    # window size defines the time sequence of the input, stride determines the stride between elements within windows, it must be 1
    ds = ds.window(window_size, shift=shift_step, stride=1, drop_remainder=True)
    # flat_map, the inouts should be tensor
    ds = ds.flat_map(lambda w: w.batch(window_size))
    #ds = ds.shuffle(shuffle_buffer) # when using LSTM (RNN), we cannot use shuffter, since it changes initial states due RNN having memory
    ds = ds.map(lambda w: (w[:,:-labels_num], w[:,-labels_num:]))
    ds = ds.batch(batch_size).prefetch(1)
    #print(list(ds.as_numpy_iterator())[0])

    # The all_ds has shape (batch_num, window_batch_num, window_size, festures_num)
    return ds




    '''
    #hift_step = window_size # this is to avoid the data from different trials in a window.
    all_ds=None
    #series = tf.expand_dims(series, axis=-1)
    for idx in range(series.shape[0]//DROPLANDING_PERIOD):
        a_trial_series = series[DROPLANDING_PERIOD*idx:DROPLANDING_PERIOD*(idx+1),:]
        ds1 = tf.data.Dataset.from_tensor_slices(a_trial_series)
        # window size defines the time sequence of the input, stride determines the stride between elements within windows, it must be 1
        ds1 = ds1.window(window_size, shift=shift_step, stride=1, drop_remainder=True)
        # flat_map, the inouts should be tensor
        ds1 = ds1.flat_map(lambda w: w.batch(window_size))
        #ds = ds.shuffle(shuffle_buffer) # when using LSTM (RNN), we cannot use shuffter, since it changes initial states due RNN having memory
        ds1 = ds1.map(lambda w: (w[:,:-labels_num], w[:,-labels_num:]))
        if(all_ds==None):
            all_ds = ds1
        else:
            all_ds = all_ds.concatenate(ds1)

    all_ds = all_ds.batch(batch_size).prefetch(1)
    #print(list(ds.as_numpy_iterator())[0])

    # The all_ds has shape (batch_num, window_batch_num, window_size, festures_num)
    return all_ds
'''


'''
Split datasets of subjects_trials_data into train, valid and test set

'''

def split_dataset(norm_subjects_trials_data, train_subject_indices, test_subject_indices, hyperparams, model_type='tf_keras', test_multi_trials=False):

    #0) subjects and their trials
    subjects_trials = hyperparams['subjects_trials']
    subject_ids_names = list(subjects_trials.keys())

    #i) subjects for train and test
    train_subject_ids_names = [subject_ids_names[subject_idx] for subject_idx in train_subject_indices]
    test_subject_ids_names = [subject_ids_names[subject_idx] for subject_idx in test_subject_indices]

    #ii) decide train and test subject dataset 
    print("Train subject set:", train_subject_ids_names, "Test subject set:", test_subject_ids_names)
    hyperparams['train_subject_ids_names'] = train_subject_ids_names
    hyperparams['test_subject_ids_names'] = test_subject_ids_names

    #iii) data from train and test subjects and their trials
    xy_train = [norm_subjects_trials_data[subject_id_name][trial].values if isinstance(norm_subjects_trials_data[subject_id_name][trial],pd.DataFrame) else  norm_subjects_trials_data[subject_id_name][trial] for subject_id_name in train_subject_ids_names for trial in subjects_trials[subject_id_name]]
    xy_valid = [norm_subjects_trials_data[subject_id_name][trial].values if isinstance(norm_subjects_trials_data[subject_id_name][trial],pd.DataFrame) else  norm_subjects_trials_data[subject_id_name][trial] for subject_id_name in test_subject_ids_names for trial in subjects_trials[subject_id_name]]

    #iV) test dataset
    if(isinstance(test_multi_trials,bool)):
        if(test_multi_trials):
            xy_test = xy_valid
        else:
            xy_test = xy_valid[0]
    elif(isinstance(test_multi_trials,int)):
        xy_test = xy_valid[0:test_multi_trials]
    else:
        print("test multiple trials is defined with wrong parameter values")
        xy_test = xy_valid[0]
        
    if(model_type=='tf_keras'):
        #iv) concate data of several trials into a numpy arrary
        xy_train = np.concatenate(xy_train, axis=0)
        xy_valid = np.concatenate(xy_valid, axis=0)
        print("Train set shape", xy_train.shape)
        print("Valid set shape", xy_valid.shape)
        
        #v) window train and test dataset
        train_set = windowed_dataset(xy_train, hyperparams)
        valid_set = windowed_dataset(xy_valid, hyperparams)

        return train_set, valid_set, xy_test

    elif(model_type=='sklearn'):
        return xy_train, xy_valid, xy_test

    else:
        print("MODEL TYPE IS WRONG")
        exit()
        


