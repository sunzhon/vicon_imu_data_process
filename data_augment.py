
#Python

"""
Description:
    This is an module to augment the training dataset by orienting the IMU initial posturess.
Author: Sun Tao
Email: suntao.hn@gmail.com
Date: 2022-08-02

"""
import sys
import os
sys.path.append("./../")


from vicon_imu_data_process.process_rawdata import *
import time
import numpy as np
from skinematics import quat, rotmat
'''
# IMU 数据增强： 利用旋转矩阵扩充原始数据集

'''
def imu_data_augment(subjects_trials_dataset, rot_mats=None,save_to_hdf5=None):
    # 旋转矩阵
    if(rot_mats==None):
        rot_mats= [rotmat.R('z',-2.5), rotmat.R('z',-1.5), rotmat.R('z',-1.0), rotmat.R('z',0), rotmat.R('z',1.0), rotmat.R('z',1.5), rotmat.R('z',2.5)]
    
    # if save to h5 file directly
    if(save_to_hdf5!=None):
        h5f = h5py.File(os.path.join(DATA_PATH, save_to_hdf5), 'a')
        h5f.attrs['subjects'] = list(subjects_trials_dataset.keys()) # subjects

    augment_subjects_trials_dataset={}
    # IMU locations
    imus = ['CHEST','WAIST','R_THIGH','R_SHANK','R_FOOT','L_THIGH','L_SHANK','L_FOOT']
    for subject_id_name in subjects_trials_dataset.keys():
        augment_subjects_trials_dataset[subject_id_name] ={}
        if(save_to_hdf5!=None): #create group if save to h5 file directly
            h5f_sub = h5f.create_group(subject_id_name)
        for trial in subjects_trials_dataset[subject_id_name].keys():
            for rot_idx, rot_mat in enumerate(rot_mats): # augment matrix
                a_trial_imu_data = []
                for a_imu in imus:
                    # A IMU data fields
                    a_imu_fields = [a_imu+ '_' + x for x in IMU_RAW_FIELDS]
                    
                    # 旋转前的IMU数据
                    acc = subjects_trials_dataset[subject_id_name][trial][a_imu_fields[:3]].values
                    vel= subjects_trials_dataset[subject_id_name][trial][a_imu_fields[3:6]].values
                    mag = subjects_trials_dataset[subject_id_name][trial][a_imu_fields[6:]].values
                
                    # 旋转后的IMU 的数据
                    aug_acc = np.dot(rot_mat, acc.T).T
                    aug_vel = np.dot(rot_mat, vel.T).T
                    aug_mag = np.dot(rot_mat, mag.T).T
                    
                    aug_a_imu_data = pd.DataFrame(data=np.hstack([aug_acc,aug_vel,aug_mag]),columns=a_imu_fields)
                    a_trial_imu_data.append(aug_a_imu_data)
                # rename trial
                a_trial_name = trial+'_'+str(rot_idx)
                # re-generated a trial of dataset
                a_trial_data =  pd.concat(a_trial_imu_data,axis=1)
                a_trial_data['TIME'] = subjects_trials_dataset[subject_id_name][trial]['TIME']
                labels_fields = list(set(subjects_trials_dataset[subject_id_name][trial].columns)- set(a_trial_data.columns))
                a_trial_data[labels_fields] = subjects_trials_dataset[subject_id_name][trial][labels_fields]
                
                '''
                augment_subjects_trials_dataset[subject_id_name][a_trial_name] = pd.concat(a_trial_imu_data,axis=1)
                augment_subjects_trials_dataset[subject_id_name][a_trial_name]['TIME'] = subjects_trials_dataset[subject_id_name][trial]['TIME']
                # find labels_fields
                labels_fields = list(set(subjects_trials_dataset[subject_id_name][trial].columns)- set(augment_subjects_trials_dataset[subject_id_name][a_trial_name].columns))
                augment_subjects_trials_dataset[subject_id_name][a_trial_name][labels_fields] = subjects_trials_dataset[subject_id_name][trial][labels_fields]
                '''

                if(save_to_hdf5==None):
                    augment_subjects_trials_dataset[subject_id_name][a_trial_name] = a_trial_data

                else:
                    try:
                        h5f_sub.create_dataset(a_trial_name, data=a_trial_data)
                        h5f_sub.attrs['columns']=list(a_trial_data.columns)
                    except Exception as e: 
                        print(e)
                        print(termcolor.colored("Subject: {} h5 file path in session: {} is wrong".format(subject_id_name, trial),'red'))

    if(save_to_hdf5==None):
        return augment_subjects_trials_dataset



def get_rotmatirx(rot_angles):
    '''
    get rotation matrix of the IMU
    
    '''
    rot_matrix= []
    assert(isinstance(rot_angles,dict))
    assert(list(rot_angles.keys())==['x_angles','y_angles','z_angles'])
    for x in rot_angles['x_angles']:
        for y in rot_angles['y_angles']:
            for z in rot_angles['z_angles']:
                rot_matrix.append(np.dot(np.dot(rotmat.R('x',x),rotmat.R('y',y)),rotmat.R('z',z)))
                
    return rot_matrix


if __name__=='__main__':
    subjects_trials = set_subjects_trials(selected=True, landing_manner='double_legs')
    subjects_trials_dataset = load_subjects_dataset(subjects_trials)
    #rot_angles = {'x_angles':np.linspace(-5,5,5),'y_angles':np.linspace(-5,5,5),'z_angles':np.linspace(-10,10,9)}
    rot_angles = {'x_angles':np.linspace(-1,1,1),'y_angles':np.linspace(-1,1,1),'z_angles':np.linspace(-1,1,1)}
    print(rot_angles['x_angles'])
    rot_matrix = get_rotmatirx(rot_angles)
    #print(rot_matrix)
    augment_subjects_trials_dataset = imu_data_augment(subjects_trials_dataset,rot_matrix,save_to_hdf5 = "5_augment_features_labels_rawdatasets")
    #save_subjects_dataset_to_a_h5(augment_subjects_trials_dataset,'4_augment_features_labels_rawdatasets.hdf5')


